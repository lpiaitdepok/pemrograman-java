import javax.swing.*;

public class MyGUIProgram4 {
	MyGUIProgram4() {
		JFrame f = new JFrame();
		
		f.setSize(100,100);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		new MyGUIProgram4();
	}
}