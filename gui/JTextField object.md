reference https://www.codejava.net/

```java
' specified textfield's initial text
JTextField textField = new JTextField("This is a text");
' specified textfield's width
JTextField textField = new JTextField(20);
' or 
' both initial text and number of columns:
JTextField textField = new JTextField("This is a text", 20);
' Create a default and empty text field then set the text and the number of columns later
JTextField textField = new JTextField();
textField.setText("This is a text");
textField.setColumns(20);

' Getting all content
String content = textField.getText();
```