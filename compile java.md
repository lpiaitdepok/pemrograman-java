# compile
## javac
javac SampleApplication.java

or

javac -d path/SampleApplication.java

or

cd path

javac SampleApplication.java

# run java
## java
java -cp . SampleApplication