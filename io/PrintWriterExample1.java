import java.io.PrintWriter;

class PrintWriterTes1 {
public static void main(String[] args) {
// second parameter in PrintWriter constructor method to 
// decrease need to flush the PrintWriter object
	PrintWriter out = new PrintWriter(System.out, true);
	out.println("Hello, world");
	out.close();
}
}