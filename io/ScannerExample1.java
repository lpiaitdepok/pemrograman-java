import java.util.Scanner; 
public class ScannerExample1 
{ 
    public static void main(String[] args) 
    { 
        // Declare the object and initialize with 
        // predefined standard input object 
        Scanner sc = new Scanner(System.in); 
  
        // String input 
        String data = sc.nextLine(); 
        // Print the values to check if input was correctly obtained. 
        System.out.println("Data: "+data); 
    } 
} 