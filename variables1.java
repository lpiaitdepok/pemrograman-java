// declare a variable
int date;

// initialize a variable
int date = 13;

// assign a variable
date = 14;

// dynamic assignment
date = date + 1;